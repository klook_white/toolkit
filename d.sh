#!/bin/bash

function get_http_code() {
  echo $(curl -I -m 10 -o /dev/null -s -w %{http_code} $1)
}

 function loading() {
   duration=$1
   text=$2
   if [[ $1 == "" ]]; then
     duration=60
   fi
   if [[ $2 == "" ]]; then
     text="waiting"
   fi
   duration=$[$duration * 2]
   i=0
   str=""
   arry=("." ".." "...")
   while [ $i -le $duration ]
   do
       let index=i%3
       printf "%s%-4s\r" "$text" "${arry[$index]}"
       sleep 0.5
       let i=i+1
   done
 }

function waiting() {
  echo "starting "$srv_name", please be patient"
  echo "it will take about 1 minutes"
  loading 40
  # sleep 60s
  echo $srv_name "maybe started. let's try ponging it"
  if [[ $1 == "serv" ]]; then
    short_srv_name=${srv_name:8}
  else
    short_srv_name=$srv_name
  fi
  pong_url="http://localhost/v1/"$short_srv_name"/pong"
  echo "pong url: "$pong_url
  #short_srv_name=${srv_name: 8}
  pong=$(get_http_code $pong_url)

  retry_count=0
  max_attempts=10
  while [[ $retry_count -lt $max_attempts && "$pong" != "200" ]]; do
    echo "oh no,pong failed,response code=$pong,repong after 8s,please be patient"
    loading 8
    # sleep 8s
    pong=$(get_http_code $pong_url)

    retry_count=$(($retry_count + 1))
  done

  if [[ "$pong" == "200" ]]; then
    echo -e "\033[31moh ho! pong successfully! \033[0m"
    echo -e "\033[31mdeployed service: $srv_name \033[0m"
    echo -e "\033[31mdeployed branch:  $branch \033[0m"
  else
    echo -e "\033[31mpong exhausted, please check the log file \033[0m"
  fi
}

function prop() {
  grep "^${1}" /root/toolkit/short-name-map.properties | cut -d'=' -f2 | sed 's/\r//'
}
srv_name=$(prop "$1")

if [ -z "$srv_name" ]; then
  echo "unknown srv key: "$1
fi
is_java=0
if [[ $srv_name == java* ]]; then
  echo "java service"
  is_java=1
  srv_name=${srv_name:4}
fi

branch=$2
if [ -z "$2" ]; then
  a=$(kcli show $srv_name)
  array=(${a//:/ })
  echo "没有指定分支，部署上次发布的分支: "${array[2]}
  branch=${array[2]}
fi

echo "kcli d "$srv_name $branch
kcli d $srv_name $branch
if [[ $is_java == 1 ]]; then
  waiting
fi
