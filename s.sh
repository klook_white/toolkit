#!/bin/bash
function prop() {
  grep "^${1}" /root/toolkit/short-name-map.properties | cut -d'=' -f2 | sed 's/\r//'
}
srv_name=$(prop "$1")

if [ -z "$srv_name" ]; then
  echo "unknown srv key: "$1
fi
is_java=0
if [[ $srv_name == java* ]]; then
  echo "java service"
  is_java=1
  srv_name=${srv_name:4}
fi

kcli show $srv_name
