#!/bin/bash

function prop() {
    grep "^${1}=" /root/toolkit/short-name-map.properties | cut -d'=' -f2 | sed 's/\r//'
}


function add_prop() {
    echo "$1"=$2 >>/root/toolkit/short-name-map.properties
    echo "successfully add short service name mapping: $1=$2"
}

operation=$1
srv_name=$(prop "$2")

if [[ $operation == add ]]; then
    if [[ -z $srv_name ]]; then
        add_prop "$2" "$3"
    else
        echo "$2" already used. full service name is ""$srv_name""
    fi
elif [[ $operation == update ]]; then
    if [[ -z $srv_name ]]; then
        echo "$2" not found. please use \'add\' operation.
    else
        sed -i "/^$2=/d" /root/toolkit/short-name-map.properties
        add_prop "$2" "$3"
    fi
elif [[ $operation == remove ]]; then
    sed -i "/^$2=/d" /root/toolkit/short-name-map.properties
else
    echo unsupport operation.
fi
