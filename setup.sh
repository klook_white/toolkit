#!/bin/bash

d=$(cat ~/.bashrc | grep ^d\(\))
if [[ -z $d ]]; then
    echo "d(){" >>~/.bashrc
    echo " . /root/toolkit/d.sh \$1 \$2" >>~/.bashrc
    echo "}" >>~/.bashrc
    echo d-script init successfully.
else
    echo d-script update successfully.
fi
s=$(cat ~/.bashrc | grep ^s\(\))
if [[ -z $s ]]; then
    echo "s(){" >>~/.bashrc
    echo " . /root/toolkit/s.sh \$1" >>~/.bashrc
    echo "}" >>~/.bashrc
    echo s-script init successfully.
else
    echo s-script update successfully.
fi
wcli=$(cat ~/.bashrc | grep ^wcli\(\))
if [[ -z $wcli ]]; then
    echo "wcli(){" >>~/.bashrc
    echo " . /root/toolkit/wcli.sh \$1 \$2 \$3" >>~/.bashrc
    echo "}" >>~/.bashrc
    echo wcli-script init successfully.
else
    echo wcli-script update successfully.
fi

source ~/.bashrc